/* Відповіді:

1.  У програмуванні функції потрібні задля більш швидкого написання коду і, як наслідок, створення більш читабельного коду. Замість багаткоратного написання 
    якоїсь одної дії, її можна записати в функцію та потім викликати її, коли необхідно. 

2.  Ми передаємо аргументи в функцію задля того, щоб вона виконувала дії, які в ній прописані.
    
3.  Оператор return повертає результат, який був виконаний у функції.
*/

let a = prompt('Insert first number');
while (a == "" || isNaN(a)) {
    a = prompt('Incorrect, insert correct first number.', a) 
}

let b = prompt('Insert second number');
while(b == "" || isNaN(b)) {
    b = prompt('Please, insert correct second number', b);
}

let c = prompt('Insert an operator');

function calc(a,b,c) {
    if(c == '+') {
        return +a + +b;
    }
    else if(c == '-') {
        return a - b;
    }
    else if(c == '*') {
        return a * b
    }
    else if(c == '/') {
        return a / b;
    }
}

let result = calc(a,b,c);
console.log(result);



